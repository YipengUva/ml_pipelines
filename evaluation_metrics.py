# -*- coding: utf-8 -*-
"""
Created on Sun Apr 18 13:41:26 2021

@author: yipeng
"""
# %% load into required pkgs and funs 
from sklearn import metrics
import pandas as pd
from sklearn import model_selection

# %% evaluation metrics for binary classification problem 
# %% define a fun to evluate a single model 
def binary_evaluation_base(y_true, y_hat, y_hat_prob):
    # predicted vs. actually outcome
    m_actual = y_true
    m_predicted = y_hat
    m_predicted_prob = y_hat_prob

    # confusion matrix
    CM = metrics.confusion_matrix(m_actual, m_predicted)   
    TN = CM[0][0]
    FN = CM[1][0]
    TP = CM[1][1]
    FP = CM[0][1]
    
    # mutiple evaluation metrics    
    sensitivity = TP / (TP + FN)
    specificity = TN / (TN + FP)
    balanced_accuracy =  (TP / (TP + FN) + TN / (TN + FP))/2 
    recall = sensitivity
    precision = TP/(TP+FP)
    f1_score = 2*TP / (2*TP + FP+FN)   
    AUC = metrics.roc_auc_score(m_actual, 
                                m_predicted_prob)
    
    # metrics
    model_metrics = pd.Series(
        [sensitivity, specificity, balanced_accuracy, recall, precision, 
         f1_score, AUC], 
        index=['sensitivity', 'specificity', 'balanced_accuracy', 
               'recall', 'precision', 'f1_score', 'AUC'])
    
    return model_metrics

# %% define a fun to evluate a single model 
def regression_evaluation_base(y_true, y_hat):
    # R2 score 
    R2_score = metrics.r2_score(y_true, y_hat)
    
    # MSE 
    MSE_error = metrics.mean_squared_error(y_true, y_hat)
    
    # MAE
    MAE_error = metrics.mean_absolute_error(y_true, y_hat)    
        
    # metrics
    model_metrics = pd.Series(
        [R2_score, MSE_error, MAE_error], 
        index=['R2', 'MSE', 'MAE'])    
    return model_metrics